package sic.test;

/**
 * Test000.java (source code) -> javac -> Test000.class (bytecode)
 * <pre>
 * cd java_basics_00/src/test/java
 * javac sic/test/Test000.java
 * java sic.test.Test000
 * </pre>
 *
 * bytecode: Zwischencode -> Java Runtime Environment (JRE) -> CPU
 */
public class Test000 {

	public static void main(final String[] args) {
		System.out.println("hallo 2");
	}

}
